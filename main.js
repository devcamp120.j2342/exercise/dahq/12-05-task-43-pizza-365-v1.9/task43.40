"use strict"
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
var gDataSizeS = "";
var gDataSizeM = "";
var gDataSizeL = "";

/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
$(document).ready(function () {
    $("#btn-size-s").on("click", function () {
        onBtnSizeS();
        console.log(gDataSizeS);
    })
    $("#btn-size-m").on("click", function () {
        onBtnSizeM();
        console.log(gDataSizeM);
    })
    $("#btn-size-l").on("click", function () {
        onBtnSizeL();
        console.log(gDataSizeL);
    })

})
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
function onBtnSizeS() {
    changeColorCombo("S");
    gDataSizeS = getDataCombo("S", "20cm", "2", "2", "200g", 150000);
}
function onBtnSizeM() {
    changeColorCombo("M");
    gDataSizeM = getDataCombo("M", "25cm", "3", "4", "300g", 200000);
}
function onBtnSizeL() {
    changeColorCombo("L");
    gDataSizeL = getDataCombo("L", "30cm", "4", "8", "500g", 250000);
}

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
function getDataCombo(paramSize, paramDuongKinh, paramNuocNgot, paramSuon, paramSalad, paramVND) {
    var vObjCombo = {
        kichCo: paramSize,
        duongKinh: paramDuongKinh,
        soLuongNuoc: paramNuocNgot,
        suon: paramSuon,
        salad: paramSalad,
        thanhTien: paramVND
    }
    return vObjCombo;
}

function changeColorCombo(paramSize) {

    if (paramSize == "S") {
        $("#btn-size-s").data("data-combo-size", "Y")
            .removeClass()
            .addClass("btn btn-success col-sm-12");
        $("#btn-size-m").data("data-combo-size", "N")
            .removeClass()
            .addClass("btn btn-warning col-sm-12");
        $("#btn-size-l").data("data-combo-size", "N")
            .removeClass()
            .addClass("btn btn-warning col-sm-12");

    }
    else if (paramSize == "M") {
        $("#btn-size-s").data("data-combo-size", "N")
            .removeClass()
            .addClass("btn btn-warning col-sm-12");
        $("#btn-size-m").data("data-combo-size", "N")
            .removeClass()
            .addClass("btn btn-success col-sm-12");
        $("#btn-size-l").data("data-combo-size", "N")
            .removeClass()
            .addClass("btn btn-warning col-sm-12");
    }
    else if (paramSize == "L") {
        $("#btn-size-s").data("data-combo-size", "N")
            .removeClass()
            .addClass("btn btn-warning col-sm-12");
        $("#btn-size-m").data("data-combo-size", "N")
            .removeClass()
            .addClass("btn btn-warning col-sm-12");
        $("#btn-size-l").data("data-combo-size", "N")
            .removeClass()
            .addClass("btn btn-success col-sm-12");
    }

}